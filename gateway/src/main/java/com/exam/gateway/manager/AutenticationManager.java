package com.exam.gateway.manager;

import com.exam.gateway.dto.User;

public interface AutenticationManager {
    String autenticationManager(User user);
}
