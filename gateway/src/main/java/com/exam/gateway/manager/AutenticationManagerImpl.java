package com.exam.gateway.manager;

import com.exam.gateway.dto.User;

public class AutenticationManagerImpl implements AutenticationManager {

    @Override
    public String autenticationManager(User user) {
        JWTGenerator jWTGenerator = new JWTGenerator();
        return jWTGenerator.getJWTToken(user.getUser());
    }
    
}
