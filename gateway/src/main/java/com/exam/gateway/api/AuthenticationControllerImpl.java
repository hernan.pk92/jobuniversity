package com.exam.gateway.api;

import com.exam.gateway.dto.User;
import com.exam.gateway.services.AuthenticationServicesImpl;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.client.RestTemplate;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;

@RestController
public class AuthenticationControllerImpl implements AuthenticationController {

    @Override
    @PostMapping("auth")
    @SuppressWarnings("null")
    @Consumes("application/json")
    @Produces("application/json")
    public ResponseEntity authenticateUser(HttpServletRequest request,
            HttpServletResponse response, @RequestBody User user) {

        JsonObject jsonObject = new JsonObject();
        Gson gson = new Gson();

        try {
            validParameters(user);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(org.springframework.http.MediaType.APPLICATION_JSON);

            String requestClient = gson.toJson(user);
            String postUrl = buildResourcePath();
            HttpEntity<String> entity = new HttpEntity<>(requestClient, headers);
            RestTemplate restTemplate = createRestTemplate(1000);
            ResponseEntity<String> postResponse = restTemplate.postForEntity(postUrl, entity, String.class);
            String responseRequest = postResponse.getBody();

            if (responseRequest.equals("false")) {
                throw new RuntimeException("El usuario o la contraseña son incorrectos");
            }

            AuthenticationServicesImpl authenticationServicesImpl = new AuthenticationServicesImpl();

            String token = authenticationServicesImpl.authenticationServices(user);

            jsonObject.addProperty("User", user.getUser());
            jsonObject.addProperty("Token", token);

        } catch (IllegalArgumentException ex) {
            jsonObject = new JsonObject();
            jsonObject.addProperty("Error", ex.getMessage());
            return new ResponseEntity(gson.toJson(jsonObject), HttpStatus.BAD_REQUEST);
        } catch (RuntimeException ex) {
            System.out.println(String.format("Ocurrio un error en la peticion: %s", ex.getMessage()));
            jsonObject = new JsonObject();
            jsonObject.addProperty("Error", ex.getMessage());
            return new ResponseEntity(gson.toJson(jsonObject), HttpStatus.BAD_REQUEST);
        } catch (Exception ex) {
            System.out.println(String.format("Ocurrio un error inesperado %s", ex.getMessage()));
            jsonObject = new JsonObject();
            jsonObject.addProperty("Error", "Ocurrio un error inesperado, vuelve a intentarlo nuevamente");
            return new ResponseEntity(gson.toJson(jsonObject), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity(gson.toJson(jsonObject), HttpStatus.OK);
    }

    private void validParameters(User user) {
        if (user.getUser() == null || user.getUser().isEmpty()) {
            throw new IllegalArgumentException("El campo usuario es obligatorio");
        }
        if (user.getPwd() == null || user.getPwd().isEmpty()) {
            throw new IllegalArgumentException("El campo contraseña es obligatorio");
        }
    }

    private String buildResourcePath() {
        String protocol = "http";
        String parentResourcePath = "/auth/user";
        //return protocol + "://" + serviceHost + ":" + servicePort + parentResourcePath + resource;
        return protocol + "://localhost:8081" + parentResourcePath;
    }

    private RestTemplate createRestTemplate(int segTimeOut) {
        RestTemplate restTemplate = new RestTemplate();

        SimpleClientHttpRequestFactory simpleClient = (SimpleClientHttpRequestFactory) restTemplate
                .getRequestFactory();
        simpleClient.setReadTimeout(segTimeOut * 1000);
        simpleClient.setConnectTimeout(segTimeOut * 1000);
        simpleClient.setOutputStreaming(false);
        return restTemplate;
    }

}
