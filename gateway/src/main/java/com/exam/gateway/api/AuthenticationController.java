package com.exam.gateway.api;

import com.exam.gateway.dto.User;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.http.ResponseEntity;

public interface AuthenticationController {

    ResponseEntity authenticateUser(HttpServletRequest request, HttpServletResponse response, User user);
}
