package com.exam.gateway.services;

import com.exam.gateway.dto.User;
import com.exam.gateway.manager.AutenticationManagerImpl;

public class AuthenticationServicesImpl implements AuthenticationServices {

    @Override
    public String authenticationServices(User user) {
        AutenticationManagerImpl autenticationManagerImpl = new AutenticationManagerImpl(); 
        return autenticationManagerImpl.autenticationManager(user);
    }
    
}
