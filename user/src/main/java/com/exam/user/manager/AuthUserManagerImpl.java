package com.exam.user.manager;

import com.exam.user.dao.AuthUserDaoImpl;
import com.exam.user.dto.UserAuth;

public class AuthUserManagerImpl implements AuthUserManager {
    
    @Override
    public boolean authManager(UserAuth userAuth) {
        boolean isValidUserArguments = validInfo(userAuth);
        
        if(!isValidUserArguments) {
            throw new RuntimeException("Hay un problema en los argumentos");
        }
        
        AuthUserDaoImpl authUserDao = new AuthUserDaoImpl();
        return authUserDao.authUserDao(userAuth);
    }
    
    private boolean validInfo(UserAuth userAuth) throws RuntimeException {
        if(userAuth.getUser() == null || userAuth.getUser().isEmpty()) {
            throw new RuntimeException("El campo usuario es obligatorio");
        }
        if(userAuth.getPwd() == null || userAuth.getPwd().isEmpty()) {
            throw new RuntimeException("El campo contraseña es obligatorio");
        }
        return true;
    }
    
}
