package com.exam.user.manager;

import com.exam.user.dto.UserAuth;

public interface AuthUserManager {
    boolean authManager(UserAuth userAuth);
}
