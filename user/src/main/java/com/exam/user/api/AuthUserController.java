package com.exam.user.api;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface AuthUserController {
    String authUser(HttpServletRequest request, HttpServletResponse response, String userInfo);
}
