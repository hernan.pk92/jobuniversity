package com.exam.user.api;

import com.exam.user.dto.UserAuth;
import com.exam.user.services.AuthUserServicesImpl;
import com.google.gson.Gson;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthUserControllerImpl implements AuthUserController {

    @Override
    @PostMapping("/auth/user")
    public String authUser(HttpServletRequest request, HttpServletResponse response, @RequestBody String userInfo) {
        String returnResponse = "true";
        try {
            Gson gson = new Gson();
            UserAuth user = gson.fromJson(userInfo, UserAuth.class);
            AuthUserServicesImpl authUserServicesImpl = new AuthUserServicesImpl();
            boolean responseAuth = authUserServicesImpl.authUserServices(user);

            if (!responseAuth) {
                throw new RuntimeException("El usuario o contraseña son incorrectos.");
            }

        } catch (RuntimeException ex) {
            System.out.println("Ocurrio un error en el sistema");
            System.out.println(ex.getMessage());
            returnResponse = "false";
        } catch (Exception ex) {
            System.out.println("Ocurrio un error en el sistema");
            System.out.println(ex.getMessage());
            returnResponse = "false";
        }

        return returnResponse;
    }

}
