package com.exam.user.dao;

import com.exam.user.dto.UserAuth;

public class AuthUserDaoImpl implements AuthUserDao{
    
    @Override
    public boolean authUserDao(UserAuth userAuth) {
        if(!userAuth.getUser().equals("test") || !userAuth.getPwd().equals("123")){
            return false;
        }
        return true;
    }
    
}
