package com.exam.user.dao;

import com.exam.user.dto.UserAuth;

public interface AuthUserDao {
    boolean authUserDao(UserAuth userAuth);
}
