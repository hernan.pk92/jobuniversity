package com.exam.user.services;

import com.exam.user.dto.UserAuth;
import com.exam.user.manager.AuthUserManagerImpl;

public class AuthUserServicesImpl implements AuthUserServices {

    @Override
    public boolean authUserServices(UserAuth userAuth) {
        AuthUserManagerImpl authUserManagerImpl = new AuthUserManagerImpl();
        return authUserManagerImpl.authManager(userAuth);
    }
    
}
